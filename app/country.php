<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class country extends Model
{
    protected $fillable = ['name', 'status_data'];
	
	static $table_name = 'countries';
}
