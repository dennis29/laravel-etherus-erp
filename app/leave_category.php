<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class leave_category extends Model
{
   protected $fillable = ['name', 'status_data'];
	
   static $table_name = 'leave_categories';
}
