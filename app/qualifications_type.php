<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class qualifications_type extends Model
{
    protected $fillable = ['name', 'status_data'];
	
	static $table_name = 'qualifications_types';
}
