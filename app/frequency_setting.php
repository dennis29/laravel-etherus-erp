<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class frequency_setting extends Model
{
    protected $fillable = ['name', 'status'];
	
	static $table_name = 'frequency_settings';
}
