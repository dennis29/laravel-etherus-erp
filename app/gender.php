<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gender extends Model
{
    protected $fillable = ['name', 'status_data'];
	
	static $table_name = 'genders';
}
