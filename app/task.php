<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class task extends Model
{
  protected $fillable = ['proj_name', 'task_priority' ,'task_desc' ,'start_date','end_date','created_by','assign_employee','status','status_data'];
	
  static $table_name = 'tasks';
}
