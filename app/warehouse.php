<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class warehouse extends Model
{
     protected $fillable = ['country_id', 'address' ,'description', 'remarks' ,'status_data'];
	
	static $table_name = 'warehouses';
}
