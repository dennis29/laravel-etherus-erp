<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employement_title extends Model
{
    protected $fillable = ['name', 'status_data'];
	
	static $table_name = 'employement_titles';
}
