<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\department_category;

use App\Main_model;

class DepartmentCategoryController extends Controller
{
    function __construct()
    {
        $this->department_category = new department_category();
	    $this->tbl = $this->department_category::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
