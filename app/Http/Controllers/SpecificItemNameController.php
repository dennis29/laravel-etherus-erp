<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\specific_item_name;

use App\Main_model;

class SpecificItemNameController extends Controller
{
    function __construct()
    {
        $this->specific_item_name = new specific_item_name();
	    $this->tbl = $this->specific_item_name::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
