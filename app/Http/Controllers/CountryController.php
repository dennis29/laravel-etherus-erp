<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\country;

use App\Main_model;

class CountryController extends Controller
{
    function __construct()
    {
        $this->country = new country();
	    $this->tbl = $this->country::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
