<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\supplier;

use App\Main_model;

class SupplierController extends Controller
{
    function __construct()
    {
        $this->supplier = new supplier();
	    $this->tbl = $this->supplier::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
