<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\task;

use App\Main_model;

class TaskController extends Controller
{
    function __construct()
    {
        $this->task = new task();
	    $this->tbl = $this->task::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
