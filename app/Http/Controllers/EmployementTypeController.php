<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\employement_type;

use App\Main_model;

class EmployementTypeController extends Controller
{
    function __construct()
    {
        $this->employement_type = new employement_type();
	    $this->tbl = $this->employement_type::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
