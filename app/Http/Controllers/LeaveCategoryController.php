<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\leave_category;

use App\Main_model;

class LeaveCategoryController extends Controller
{
    function __construct()
    {
        $this->leave_category = new leave_category();
	    $this->tbl = $this->leave_category::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
