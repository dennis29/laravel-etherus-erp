<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\item_category;

use App\Main_model;

class ItemCategoryController extends Controller
{
    function __construct()
    {
        $this->item_category = new item_category();
	    $this->tbl = $this->item_category::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
