<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\motor_type;

use App\Main_model;

class MotorTypeController extends Controller
{
    function __construct()
    {
        $this->motor_type = new motor_type();
	    $this->tbl = $this->motor_type::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
