<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\warehouse;

use App\Main_model;

class WarehouseController extends Controller
{
    function __construct()
    {
        $this->warehouse = new warehouse();
	    $this->tbl = $this->warehouse::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
