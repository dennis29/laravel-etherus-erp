<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\nationality;

use App\Main_model;

class NationalityController extends Controller
{
   function __construct()
    {
        $this->nationality = new nationality();
	    $this->tbl = $this->nationality::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
