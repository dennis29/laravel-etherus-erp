<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\religion;

use App\Main_model;

class ReligionController extends Controller
{
    function __construct()
    {
        $this->religion = new religion();
	    $this->tbl = $this->religion::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
