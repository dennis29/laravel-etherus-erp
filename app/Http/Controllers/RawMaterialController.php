<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\raw_material;

use App\Main_model;

class RawMaterialController extends Controller
{
    function __construct()
    {
        $this->raw_material = new raw_material();
	    $this->tbl = $this->raw_material::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
