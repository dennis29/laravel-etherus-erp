<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\academy_type;

use App\Main_model;

class AcademyTypeController extends Controller
{
    function __construct()
    {
        $this->academy_type = new academy_type();
	    $this->tbl = $this->academy_type::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
