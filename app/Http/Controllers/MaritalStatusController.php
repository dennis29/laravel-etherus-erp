<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\marital_status;

use App\Main_model;

class MaritalStatusController extends Controller
{
	
	function __construct()
    {
        $this->marital_status = new marital_status();
	    $this->tbl = $this->marital_status::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
	}
}