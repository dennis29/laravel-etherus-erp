<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\claims_category;

use App\Main_model;

class ClaimsCategoryController extends Controller
{
    function __construct()
    {
        $this->claims_category = new claims_category();
	    $this->tbl = $this->claims_category::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
