<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public function main_index()
	{
		$tbl = $this->tbl;
		return view($tbl.'/index')->with(compact('tbl'));
	}
	
	public function load_data()
	{
		return response()->json($this->main_model->load_data());	
	}
	
	public function load_data_warehouse()
	{
		return response()->json($this->main_model->load_data_warehouse());	
	}
	
	public function load_raw_material()
	{
		return response()->json($this->main_model->load_raw_material());	
	}
	
	public function load_purchase_order()
	{
		return response()->json($this->main_model->load_purchase_order());	
	}
	
	public function load_customer()
	{
		return response()->json($this->main_model->load_customer());	
	}
	
	public function load_data_per_purchase_order_list(Request $request)
	{
		return response()->json($this->main_model->load_data_per_purchase_order_list($request->post('purchase_order_id')));	
	}
	
	public function get_by(Request $request)
	{
		return response()->json($this->main_model->get_by($request->post('id')));
	}
	
	public function get_warehouse(Request $request)
	{
		return response()->json($this->main_model->get_warehouse($request->post('id')));
	}
	
	public function get_raw_material(Request $request)
	{
		return response()->json($this->main_model->get_raw_material($request->post('id')));
	}
	
	public function get_purchase_order(Request $request)
	{
		return response()->json($this->main_model->get_purchase_order($request->post('id')));
	}
	
	public function get_customer(Request $request)
	{
		return response()->json($this->main_model->get_customer($request->post('id')));
	}
	
	public function insert(Request $request)
	{
		return response()->json($this->main_model->insert($request->post()));
	}
	
	public function soft_delete(Request $request)
	{
		return response()->json($this->main_model->soft_delete($request->post('id')));
	}
	
	public function find_country(Request $request)
	{
		return response()->json($this->main_model->find_country($request->post('key')));
	}
	
	public function find_item_category(Request $request)
	{
		return response()->json($this->main_model->find_item_category($request->post('key')));
	}
	
	public function find_specific_item_name(Request $request)
	{
		return response()->json($this->main_model->find_specific_item_name($request->post('key')));
	}
	
	public function find_supplier(Request $request)
	{
		return response()->json($this->main_model->find_supplier($request->post('key')));
	}
	
	public function find_purchase_order(Request $request)
	{
		return response()->json($this->main_model->find_purchase_order($request->post('key')));
	}
}
