<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\qualifications_type;

use App\Main_model;

class QualificationsTypeController extends Controller
{
    function __construct()
    {
        $this->qualifications_type = new qualifications_type();
	    $this->tbl = $this->qualifications_type::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
