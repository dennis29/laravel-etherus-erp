<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\attendance_setting;

use App\Main_model;

class AttendanceSettingController extends Controller
{
    function __construct()
    {
        $this->attendance_setting = new attendance_setting();
	    $this->tbl = $this->attendance_setting::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
