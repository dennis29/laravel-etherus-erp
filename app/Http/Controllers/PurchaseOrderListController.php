<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\purchase_order_list;

use App\Main_model;

class PurchaseOrderListController extends Controller
{
    function __construct()
    {
        $this->purchase_order_list = new purchase_order_list();
	    $this->tbl = $this->purchase_order_list::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
