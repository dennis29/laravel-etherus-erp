<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\gender;

use App\Main_model;

class GenderController extends Controller
{
    function __construct()
    {
        $this->gender = new gender();
	    $this->tbl = $this->gender::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
	
    public function main_index()
	{
		$tbl = $this->tbl;
		return view($tbl.'/index')->with(compact('tbl'));
	}
	
	public function load_data()
	{
		return response()->json($this->main_model->load_data());	
	}
	
	public function get_by(Request $request)
	{
		return response()->json($this->main_model->get_by($request->post('id')));
	}
	
	public function insert(Request $request)
	{
		return response()->json($this->main_model->insert($request->post()));
	}
	
	public function soft_delete(Request $request)
	{
		return response()->json($this->main_model->soft_delete($request->post('id')));
	}
}
