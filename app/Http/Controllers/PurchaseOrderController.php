<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\purchase_order;

use App\Main_model;

class PurchaseOrderController extends Controller
{
    function __construct()
    {
        $this->purchase_order = new purchase_order();
	    $this->tbl = $this->purchase_order::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
