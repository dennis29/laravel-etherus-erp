<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\message_target_setting;

use App\Main_model;

class MessageTargetSettingController extends Controller
{
    function __construct()
    {
	    $this->message_target_setting = new message_target_setting();
	    $this->tbl = $this->message_target_setting::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
