<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\appraisal_setting;

use App\Main_model;

class AppraisalSettingController extends Controller
{
    function __construct()
    {
        $this->appraisal_setting = new appraisal_setting();
	    $this->tbl = $this->appraisal_setting::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
	
}
