<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\customer;

use App\Main_model;

class CustomerController extends Controller
{
    function __construct()
    {
        $this->customer = new customer();
	    $this->tbl = $this->customer::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
