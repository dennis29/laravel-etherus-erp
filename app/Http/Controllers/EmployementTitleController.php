<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\employement_title;

use App\Main_model;

class EmployementTitleController extends Controller
{
    function __construct()
    {
        $this->employement_title = new employement_title();
	    $this->tbl = $this->employement_title::$table_name;
		$this->main_model = new Main_model();
		$this->main_model::$table_name = $this->tbl; 
    }
}
