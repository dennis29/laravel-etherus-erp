<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ethnics_group extends Model
{
    protected $fillable = ['name', 'status'];
	
	static $table_name = 'ethnics_groups';
}
