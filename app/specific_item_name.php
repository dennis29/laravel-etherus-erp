<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class specific_item_name extends Model
{
    protected $fillable = ['name', 'status_data'];
	
	static $table_name = 'specific_item_names';
}
