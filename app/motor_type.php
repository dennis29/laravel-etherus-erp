<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class motor_type extends Model
{
    protected $fillable = ['name', 'status_data'];
	
	static $table_name = 'motor_types';
}
