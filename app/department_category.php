<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class department_category extends Model
{
    protected $fillable = ['name', 'status_data'];
	
	static $table_name = 'department_categories';
}
