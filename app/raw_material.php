<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class raw_material extends Model
{
    protected $fillable = ['item_id','item_category_id','specific_item_name_id','desc','qty','remarks','status_data'];
	
	static $table_name = 'raw_materials';
}
