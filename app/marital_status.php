<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class marital_status extends Model
{
	protected $fillable = ['name', 'status_data'];
	
	static $table_name = 'marital_statuses';
	
}
