<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchase_order_list extends Model
{
   protected $fillable = ['purchase_order_id', 'raw_material_id' ,'qty' ,'total','status_data'];
	
   static $table_name = 'purchase_order_lists';
}
