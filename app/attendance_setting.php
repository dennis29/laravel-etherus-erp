<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class attendance_setting extends Model
{
    protected $fillable = ['name', 'status_data'];
	
	static $table_name = 'attendance_settings';
}
