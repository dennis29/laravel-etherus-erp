<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Main_model extends Model
{
   static $table_name = "";
   function __construct()
   {
        parent::__construct();
   }
   
   function load_data(){
	   return DB::table($this::$table_name)->select('*')->where('status_data',1)->orderBy('id','desc')->get();
   }
   
   function load_data_warehouse()
    {
	//	DB::enableQueryLog();
        return DB::table('warehouses')->select(['warehouses.*','countries.name as country_name'])
            ->join('countries','countries.id','=','warehouses.country_id')
            ->where('warehouses.status_data', 1)->where('countries.status_data', 1)->orderBy('warehouses.id', 'DESC')
            ->get();
			
	//    dd(DB::getQueryLog());
    }
	
	function load_raw_material()
    {
        return DB::table('raw_materials')->select(['raw_materials.*','item_categories.name as item_category_name','specific_item_names.name as specific_item_name'])
            ->leftjoin('item_categories', 'item_categories.id','=', 'raw_materials.item_category_id')
			->leftjoin('specific_item_names', 'specific_item_names.id','=', 'raw_materials.specific_item_name_id')
            ->where('specific_item_names.status_data', 1)->where('raw_materials.status_data', 1)->where('item_categories.status_data', 1)
			->orderBy('raw_materials.id', 'DESC')
            ->get();
    }
	
	function load_purchase_order()
    {
        return DB::table('purchase_orders')->select(['purchase_orders.*','suppliers.company_name as company_name'])
            ->leftjoin('suppliers', 'suppliers.id','=', 'purchase_orders.supplier_id')
		    ->where('purchase_orders.status_data', 1)->where('suppliers.status_data', 1)
			->orderBy('purchase_orders.id', 'DESC')
            ->get();
    }
	
	function load_customer()
    {
        return DB::table('customers')->select(['customers.*','countries.name as country_name'])
            ->leftjoin('countries', 'countries.id','=', 'customers.country_id')
		    ->where('customers.status_data', 1)->where('countries.status_data', 1)
			->orderBy('customers.id', 'DESC')
            ->get();
    }
	
	function load_data_per_purchase_order_list($purchase_order_id='')
    {
        return DB::table('purchase_order_lists')->select(['purchase_order_lists.*','raw_materials.item_id as raw_material_name','purchase_orders.PO_number as purchase_order_number'])
            ->leftjoin('raw_materials', 'raw_materials.id','=', 'purchase_order_lists.raw_material_id')
            ->leftjoin('purchase_orders', 'purchase_orders.id','=','purchase_order_lists.purchase_order_id')
            ->where('purchase_order_lists.purchase_order_id', $purchase_order_id)->where('purchase_order_lists.status_data', 1)
            ->where('raw_materials.status_data', 1)->where('purchase_orders.status_data', 1)->orderBy('purchase_order_lists.id', 'DESC')
            ->get();
    }
	
    function insert($post_data=array()){
	   unset($post_data['_token']);
		if($post_data['id'])
		{
		    if($post_data['id']!="")
            {
                return DB::table($this::$table_name)->where('id',$post_data['id'])->update($post_data);
            }
		}
		else
		{
		  unset($post_data['id']);	
		  $post_data['status_data']=1;
		  return DB::table($this::$table_name)->insert($post_data);
		}
	}
	
	function get_by($id=''){
        return DB::table($this::$table_name)->select('*')->where('id',$id)->get();
    }
	
	function soft_delete($id=''){
		$data = array('status_data'=>0);
		return DB::table($this::$table_name)->where('id',$id)->update($data);
	}
	
	function find_country($key=''){
        return DB::table('countries')->select('*')->orWhere('name', 'like', '%' . $key . '%')->where('status_data',1)
            ->orderBy('id','DESC')->get();
    }
	
	function find_item_category($key=''){
        return DB::table('item_categories')->select('*')->orWhere('name', 'like', '%' . $key . '%')->where('status_data',1)
            ->orderBy('id','DESC')->get();
        //echo $this->db->last_query();die()
    }
	
	function find_supplier($key=''){
        return DB::table('suppliers')->select('*')->where('company_name', 'like', "%$key%")->where('status_data',1)
            ->orderBy('id','DESC')->get();
    }
	
	function find_specific_item_name($key=''){
        return DB::table('specific_item_names')->select('*')->orWhere('name', 'like', '%' . $key . '%')->where('status_data',1)
            ->orderBy('id','DESC')->get();
        //echo $this->db->last_query();die()
    }
	
	function find_purchase_order($key=''){
        return DB::table('purchase_orders')->select('*')->orWhere('PO_number', 'like', '%' . $key . '%')->where('status_data',1)
            ->orderBy('id','DESC')->get();
        //echo $this->db->last_query();die()
    }
	
	function get_warehouse($id='')
    {
        return DB::table('warehouses')->select(['warehouses.*','countries.name as country_name'])
            ->leftJoin('countries', 'countries.id', '=' ,'warehouses.country_id')->where('warehouses.id',$id)
            ->where('warehouses.status_data', 1)->where('countries.status_data', 1)->orderBy('warehouses.id', 'DESC')
            ->get();
        //echo $this->db->last_query();
    }
	
	function get_raw_material($id='')
    {
        return DB::table('raw_materials')->select(['raw_materials.*','item_categories.name as item_category_name','specific_item_names.name as specific_item_name'])
		    ->leftJoin('item_categories', 'item_categories.id', '=' ,'raw_materials.item_category_id')
			->leftJoin('specific_item_names', 'specific_item_names.id', '=' ,'raw_materials.specific_item_name_id')->where('raw_materials.id',$id)
            ->where('raw_materials.status_data', 1)->where('item_categories.status_data', 1)->where('specific_item_names.status_data', 1)
			->orderBy('raw_materials.id', 'DESC')
            ->get();
    }
	
	function get_purchase_order($id='')
    {
        return DB::table('purchase_orders')->select(['purchase_orders.*','suppliers.company_name as company_name'])
            ->leftJoin('suppliers', 'suppliers.id', '=' ,'purchase_orders.supplier_id')->where('purchase_orders.id',$id)
            ->where('suppliers.status_data', 1)->where('purchase_orders.status_data', 1)->orderBy('purchase_orders.id', 'DESC')
            ->get();
        //echo $this->db->last_query();
    }
	
	function get_customer($id='')
    {
		return DB::table('customers')->select(['customers.*','countries.name as country_name'])
            ->leftjoin('countries', 'countries.id','=', 'customers.country_id')->where('customers.id',$id)
		    ->where('customers.status_data', 1)->where('countries.status_data', 1)
            ->get();	
        //echo $this->db->last_query();
    }
}