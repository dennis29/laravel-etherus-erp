<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class appraisal_setting extends Model
{
   protected $fillable = ['name', 'status_data'];
	
   static $table_name = 'appraisal_settings';
}
