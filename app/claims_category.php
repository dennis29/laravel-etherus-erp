<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class claims_category extends Model
{
   protected $fillable = ['name', 'status_data'];
	
   static $table_name = 'claims_categories';
}
