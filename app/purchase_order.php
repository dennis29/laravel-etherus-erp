<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchase_order extends Model
{
    protected $fillable = ['PO_number','supplier_id','status_data'];
	
	static $table_name = 'purchase_orders';
}
