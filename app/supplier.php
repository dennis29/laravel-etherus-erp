<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class supplier extends Model
{
  protected $fillable = ['uen', 'company_name' ,'sup_name' ,'mobile_no','fax','email','company_add','remarks','gst','postal','status_data'];
	
  static $table_name = 'suppliers';
}
