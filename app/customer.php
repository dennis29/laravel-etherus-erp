<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
   protected $fillable = ['uen', 'company_name' ,'contact_person' ,'mobile_no','fax','email','company_add','country_id','postal','status_data'];
	
   static $table_name = 'customers';
}
