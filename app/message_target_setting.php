<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class message_target_setting extends Model
{
    protected $fillable = ['name', 'status'];
	
	static $table_name = 'message_target_settings';
}
