<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class academy_type extends Model
{
		protected $fillable = ['name', 'status_data'];
		
		static $table_name = 'academy_types';
}
