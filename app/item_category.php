<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class item_category extends Model
{
   protected $fillable = ['name', 'status_data'];
	
   static $table_name = 'item_categories';
}
