<html>
<head>
<title>ERP Etherus</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo_company.png')}}" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/menu.css')}}">
    
	<script type="text/javascript" src="{{asset('assets/js/jquery-1.9.0.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/function.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/function_.js')}}"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<style>
.form-inline .form-group{
	margin-bottom:20px;
	margin-left:20px;
}
.btn-primary{
    margin-bottom: 10px;
}
ul.sub-menu{
	width:170px;
}
body{
	width:1000x;
    background-color: #93a610c9;
}

.inner{
    margin-right: 100px;
}


#main-menu > li.parent{
	width: 140px
}

input{
    margin-top: 5px;

}

.btn-primary{
    margin-top: -10px;
}
</style>
<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script><div id="wrap" style="margin-bottom:50px">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css" rel="stylesheet"/>
	<header>
		<div class="inner relative">
			<a class="logo" href="#"><img src="{{asset('assets/images/logo_company.png')}}" style="width:210px;margin-bottom:20px"></a>
			<a id="menu-toggle" class="button dark" href="#"><i class="icon-reorder"></i></a>
			<nav id="navigation">
				<ul id="main-menu">
					<li class="current-menu-item" style="margin-left:-1200px"><a href="{{url('')}}?>">Home</a></li>
					<li class="parent">
						<a href="#">Management</a>
						<ul class="sub-menu">
                            <li>
                                <a class="parent" href="#"><i class="icon-file-alt"></i>Setting</a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('frequency_settings') }}"><i class="icon-wrench"></i>Frequency</a></li>
                                    <li><a href="{{url('message_target_settings') }}"><i class="icon-wrench"></i>Target Message</a></li>
                                    <li><a href="{{url('task_priority_alert') }}"><i class="icon-wrench"></i>Task Priority Alert</a></li>
                                </ul>
                            </li>
							<li><a href="{{url('project') }}"><i class="icon-wrench"></i>Project</a></li>
							<li><a href="{{url('tasks') }}"><i class="icon-wrench"></i>Task</a></li>
						</ul>
					</li>
			        <li class="parent">
						<a href="#">Production</a>
						<ul class="sub-menu">
                            <li>
                                <a class="parent" href="#"><i class="icon-file-alt"></i>Setting</a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('customers') }}"><i class="icon-wrench"></i>Customer</a></li>
                                    <li><a href="{{url('motor_types') }}"><i class="icon-wrench"></i>Motor Type</a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="parent" href="#"><i class="icon-file-alt"></i>Production Design</a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('production_design') }}"><i class="icon-wrench"></i>Production Design</a></li>
                                    <li><a href="{{url('production_design_detail') }}"><i class="icon-wrench"></i>Production Design Detail</a></li>
                                </ul>
                            </li>
						</ul>
					</li>
					<li class="parent">
						<a href="#">Inventory</a>
						<ul class="sub-menu">
                            <li>
                                <a class="parent" href="#"><i class="icon-file-alt"></i>Setting</a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('item_categories')}}"><i class="icon-wrench"></i>Item Category</a></li>
                                    <li><a href="{{url('specific_item_names') }}"><i class="icon-wrench"></i>Specific Item Name</a></li>
                                </ul>
                            </li>
							<li><a href="{{url('warehouses') }}"><i class="icon-wrench"></i>Warehouses</a></li>
							<li><a href="{{url('suppliers')}}"><i class="icon-credit-card"></i>Supplier</a></li>
							<li><a href="{{url('raw_materials')}}"><i class="icon-credit-card"></i>Raw Material Items</a></li>
						    <li>
                                <a class="parent" href="#"><i class="icon-file-alt"></i>Purchasing Order</a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('purchase_orders') }}"><i class="icon-wrench"></i>Purchasing Order</a></li>
                                    <li><a href="{{url('purchase_order_lists') }}"><i class="icon-wrench"></i>Purchasing Order List</a></li>
                                </ul>
                            </li>
						</ul>
					</li>
					<li class="parent">
						<a href="#">Human Resources</a>
						<ul class="sub-menu">
                            <li>
                                <a class="parent" href="#"><i class="icon-file-alt"></i>Setting Control</a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="parent" href="#"><i class="icon-file-alt"></i>Employee</a>
                                        <ul class="sub-menu">
                                            <li><a href="{{url('marital_statuses') }}"><i class="icon-wrench"></i>Marital Status</a></li>
                                            <li><a href="{{url('nationalities') }}"><i class="icon-wrench"></i>Nationality</a></li>
                                            <li><a href="{{url('religions') }}"><i class="icon-wrench"></i>Religion</a></li>
                                            <li><a href="{{url('ethnics_groups') }}"><i class="icon-wrench"></i>Ethnics Group</a></li>
                                            <li><a href="{{url('department_categories') }}"><i class="icon-wrench"></i>Department Category</a></li>
                                            <li><a href="{{url('employement_titles') }}"><i class="icon-wrench"></i>Employment Title</a></li>
                                            <li><a href="{{url('employement_types') }}"><i class="icon-wrench"></i>Employment Type</a></li>
                                            <li><a href="{{url('qualifications_types') }}"><i class="icon-wrench"></i>Qualifications Type</a></li>
                                            <li><a href="{{url('academy_types') }}"><i class="icon-wrench"></i>Academy Type</a></li>
                                            <li><a href="{{url('genders') }}"><i class="icon-wrench"></i>Gender</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{url('countries') }}"><i class="icon-wrench"></i>Country</a></li>
                                    <li><a href="{{url('attendance_settings') }}"><i class="icon-wrench"></i>Attendance Setting</a></li>
                                    <li>
                                        <a class="parent" href="#"><i class="icon-file-alt"></i>Payroll</a>
                                        <ul class="sub-menu">
                                            <li><a href="{{url('payroll_setting') }}"><i class="icon-wrench"></i>Payroll</a></li>
                                            <li><a href="{{url('cpf_contribution_setting') }}"><i class="icon-wrench"></i>CPF Contribution Setting</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{url('claims_categories') }}"><i class="icon-wrench"></i>Claims Category</a></li>
                                    <li><a href="{{url('leave_categories') }}"><i class="icon-wrench"></i>Leaves Category</a></li>
                                    <li><a href="{{url('appraisal_settings') }}"><i class="icon-wrench"></i>Atppraisal Setting</a></li>
                                </ul>
                            </li>
                            <li><a href="{{url('employee') }}"><i class="icon-wrench"></i>Employee</a></li>
                            <li><a href="{{url('leaves') }}"><i class="icon-wrench"></i>Leaves</a></li>
							<li><a href="{{url('overtime') }}"><i class="icon-wrench"></i>Overtime</a></li>
                            <li>
                                <a class="parent" href="#"><i class="icon-file-alt"></i>Attendance</a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('attendance') }}"><i class="icon-wrench"></i>Attendance Input</a></li>
                                    <li><a href="{{url('attendance_by_month') }}"><i class="icon-wrench"></i>Attendance Choose Month</a></li>
                                </ul>
                            </li>
							<li><a href="{{url('cpf_contribution') }}"><i class="icon-wrench"></i>CPF Contribution</a></li>
							<li><a href="{{url('aws') }}"><i class="icon-wrench"></i>AWS</a></li>
							<li><a href="{{url('qualification') }}"><i class="icon-wrench"></i>Qualification</a></li>
							<li><a href="{{url('certification') }}"><i class="icon-wrench"></i>Certification</a></li>
                            <li><a href="{{url('appraisal') }}"><i class="icon-wrench"></i>Appraisals</a></li>
                            <li><a href="{{url('claims') }}"><i class="icon-wrench"></i>Claims</a></li>
                            <li><a href="{{url('payroll') }}"><i class="icon-wrench"></i>Payroll</a></li>


						</ul>
					</li>
					<li class="parent">
						<a href="#">Administrator</a>
						<ul class="sub-menu">
							<li><a href="{{url('role_category') }}"><i class="icon-wrench"></i>Roles Category</a></li>
						</ul>
					</li>
				</ul>
			</nav>
			<div class="clear"></div>
		</div>
	</header>	
</div>
<div class="alert alert-info" id="success" style="display:none">
  <strong>Success Save Data!</strong>
</div>
<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
@yield('content')
</body></html>