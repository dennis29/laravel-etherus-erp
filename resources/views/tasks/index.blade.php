@extends('layout.layouts')
@section('content')
<div class="panel panel-default" style="margin-left:20px">
<div class="panel-heading" style="margin-bottom:30px"><h3>Task</h3></div>
  <div class="panel-body">
	 <form class="form-inline" id="frm_{{$tbl}}">
	 {{ csrf_field() }} 
		  <div class="form-group">
			<label for="proj_name">Project Name</label>
			<input type="hidden" class="form-control" id="id" value="" name="id">
			<input type="text" class="form-control" id="proj_name" name="proj_name" placeholder="Project Name" required>
		  </div>
		  <div class="form-group">
			<label for="task_priority">Task Priority</label>
			<div class="form-group">
			  <input type="text" class="form-control" id="task_priority" name="task_priority" placeholder="Task Priority" required>
			</div>
		  </div>
		  <div class="form-group">
			<label for="task_desc">Task Description</label>
			<input type="text" class="form-control" id="task_desc" name="task_desc" placeholder="Task Description" required>
		  </div>
		  <div class="form-group">
			<label for="start_date">Start Date</label>
			<input type="text" class="form-control" id="start_date" name="start_date" placeholder="Start Date" required>
		  </div>
		  <div class="form-group">
			<label for="end_date">End Date</label>
			<input type="text" class="form-control" id="end_date" name="end_date" placeholder="End Date" required>
		  </div>
		  <div class="form-group">
			<label for="created_by ">Created By</label>
			<input type="text" class="form-control" id="created_by" name="created_by" placeholder="Created By" required>
		  </div>
		  <div class="form-group">
			<label for="assign_employee">Assigned Employee</label>
			<input type="text" class="form-control" id="assign_employee" name="assign_employee" placeholder="Assigned Employee" required>
		  </div>
		  <div class="form-group">
			<label for="status">Status</label>
			<input type="text" class="form-control" id="status" name="status" placeholder="Status" required>
		  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
	</form> 
	<table class="table table-hover" id="tbl_{{$tbl}}">
	  <thead>
		<tr>
		  <th scope="col">#</th>
		  <th scope="col">Action</th>
		  <th scope="col">Project Name</th>
		  <th scope="col">Task Priority</th>
		  <th scope="col">Task Description</th>
		  <th scope="col">Start Date</th>
		  <th scope="col">End Date</th>
		  <th scope="col">Created By</th>
		  <th scope="col">Assigned Employee</th>
		  <th scope="col">Status</th>
		</tr>
	  </thead>
	  <tbody>
	  </tbody>
	</table>
  </div>
</div>
<script>
var nm = '{{$tbl}}';
var tbl = "tbl_"+nm;
var frm = "frm_"+nm;

$("#"+frm).submit(function(event){
    event.preventDefault();
	 var form = $(this);

    $.ajax({
           type: "POST",
           url: nm+'/insert',
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
			   
			   display_success();
               load_data();
           }
    });
});

function load_data(){
	
	 $.ajax({
           type: "POST",
           url: nm+'/load_data',
		   data :{
			   "_token": "{{ csrf_token() }}",
		   },
           success: function(data)
           {
      	   var table = "";
			   $.each(data, function( index, value ) {
				  table += "<tr>";
				  table += "<td>"+(index+1)+"</td>";
				  table +='<td><button type="button" class="btn btn-success btn-sm" onclick="get_by('+value.id+')" style="margin-right:10px"><span class="glyphicon glyphicon-pencil"></span> Edit';
				  table +='</button><button type="button" class="btn btn-danger btn-sm" onclick="soft_delete('+value.id+')"><span class="glyphicon glyphicon-trash"></span> Delete';
				  table +='</button></td>';
				  table += "<td>"+value.proj_name+"</td>";
				  table += "<td>"+value.task_priority+"</td>";
				  table += "<td>"+value.task_desc+"</td>";
				  table += "<td>"+value.start_date+"</td>";
				  table += "<td>"+value.end_date+"</td>";
				  table += "<td>"+value.created_by+"</td>";
				  table += "<td>"+value.assign_employee+"</td>";
				  table += "<td>"+value.status+"</td>";
				  table += "</tr>";
			   });
			   
			   $('#'+tbl+' > tbody').html(table);
			   
			   
			   
           }
    });
}

function soft_delete(id_data=''){
	
	$.ajax({
           type: "POST",
           url: nm+"/soft_delete",
           data: {id : id_data,"_token": "{{ csrf_token() }}"},
           success: function(data)
           {	
	           load_data();
           }
    });
}

function get_by(id_data=''){
	
	$.ajax({
           type: "POST",
           url: nm+'/get_by',
           data: {id : id_data,"_token": "{{ csrf_token() }}"},
           success: function(data)
           {	
	     	   $.each(data[0], function( k, v ) {
				  $('#'+frm+' #'+k).val(v)
			   });
			   
	       }
    });
}

$( "#start_date").datepicker({ dateFormat: 'yy-mm-dd' }).val();

$( "#end_date").datepicker({ dateFormat: 'yy-mm-dd' }).val();



load_data();
</script>
@endsection