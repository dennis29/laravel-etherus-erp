@extends('layout.layouts')
@section('content')
<div class="panel panel-default" style="margin-left:20px">
<div class="panel-heading" style="margin-bottom:30px"><h3>Purchase Order List</h3></div>
  <div class="panel-body">
	 <form class="form-inline" id="frm_{{$tbl}}">
	 {{ csrf_field() }} 
		  <div class="form-group">
             <label for="employee_name">Purchase Order Number</label>
             <div class="form-group">
                 <input type="hidden" class="form-control" style="margin-bottom: -10px;" id="sn" name="sn" required>
                 <input type="hidden" class="form-control" style="margin-bottom: -10px;" id="purchase_order_id" name="purchase_order_id" required>
                 <input type="text" class="form-control" style="margin-bottom: -10px;" id="purchase_order_number" placeholder="Type To Find" required>
             </div>
         </div>
         <div class="form-group">
             <label for="employee_name">Raw Material</label>
             <div class="form-group">
                 <input type="hidden" class="form-control" style="margin-bottom: -10px;" id="id" name="id" required>
                 <input type="hidden" class="form-control" style="margin-bottom: -10px;" id="raw_material_id" name="raw_material_id" required>
                 <input type="text" class="form-control" style="margin-bottom: -10px;" id="raw_material_name" placeholder="Type To Find" required>
             </div>
         </div>
         <div class="form-group">
             <label for="balance">Quantity</label>
             <input type="number" style="text-align:right" class="form-control" id="qty" name="qty" placeholder="Balance" required>
         </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
	</form> 
	<table class="table table-hover" id="tbl_{{$tbl}}">
	  <thead>
		<tr>
		  <th scope="col">#</th>
		  <th scope="col">Action</th>
		  <th scope="col">Purchase Order</th>
          <th scope="col">Raw Material</th>
		  <th scope="col">Qty</th>
		</tr>
	  </thead>
	  <tbody>
	  </tbody>
	</table>
  </div>
</div>
<script>
var nm = '{{$tbl}}';
var tbl = "tbl_"+nm;
var frm = "frm_"+nm;

$("#"+frm).submit(function(event){
    event.preventDefault();
	 var form = $(this);

    $.ajax({
           type: "POST",
           url: nm+'/insert',
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
			   
			   display_success();
               load_data();
           }
    });
});

function load_data(){
	
	 $.ajax({
           type: "POST",
           url: nm+'/load_data_per_purchase_order_list',
		   data :{
			   purchase_order_id : $('#purchase_order_id').val(),
			   "_token": "{{ csrf_token() }}"
		   },
           success: function(data)
           {
      	   var table = "";
			   $.each(data, function( index, value ) {
				  table += "<tr>";
				  table += "<td>"+(index+1)+"</td>";
				  table +='<td><button type="button" class="btn btn-success btn-sm" onclick="get_by('+value.id+')" style="margin-right:10px"><span class="glyphicon glyphicon-pencil"></span> Edit';
				  table +='</button><button type="button" class="btn btn-danger btn-sm" onclick="soft_delete('+value.id+')"><span class="glyphicon glyphicon-trash"></span> Delete';
				  table +='</button></td>';
				  table += "<td>"+value.purchase_order_number+"</td>";
				  table += "<td>"+value.raw_material_name+"</td>";
				  table += "<td>"+value.qty+"</td>";
				  table += "</tr>";
			   });
			   
			   $('#'+tbl+' > tbody').html(table);
			   
			   
			   
           }
    });
}

function soft_delete(id_data=''){
	
	$.ajax({
           type: "POST",
           url: nm+"/soft_delete",
           data: {id : id_data,"_token": "{{ csrf_token() }}"},
           success: function(data)
           {	
	           load_data();
           }
    });
}

function get_by(id_data=''){
	
	$.ajax({
           type: "POST",
           url: nm+'/get_purchase_order',
           data: {id : id_data,"_token": "{{ csrf_token() }}"},
           success: function(data)
           {	
	     	   $.each(data[0], function( k, v ) {
				  $('#'+frm+' #'+k).val(v)
			   });
			   
	       }
    });
}

$("#supplier_name").autocomplete({
    source: function (request, response) {
        $.ajax({
            type: "POST",
            url: nm+"/find_supplier",
            dataType: "json",
            data:{
                key : $('#supplier_name').val(),"_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                response($.map(data, function (item) {
                    return {
                        label: item.company_name,
                        value: item.company_name,
                        id: item.id
                    };
                }));
            }
        });
    },
    minLength: 1,
    select: function (event, ui) {
        $("#supplier_id").val("");
        $("#supplier_id").val(ui.item.id);
    }
});

$("#purchase_order_number").autocomplete({
    source: function (request, response) {
        $.ajax({
            type: "POST",
            url: nm+"/find_purchase_order",
            dataType: "json",
            data:{
                key : $('#purchase_order_number').val(),
				"_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                response($.map(data, function (item) {
                    return {
                        label: item.PO_number,
                        value: item.PO_number,
                        id: item.id
                    };
                }));
            }
        });
    },
    minLength: 1,
    select: function (event, ui) {
        $('input').val('');
        $("#purchase_order_id").val(ui.item.id);
        load_data()
    }
});

$("#raw_material_name").autocomplete({
    source: function (request, response) {
        $.ajax({
            type: "POST",
            url: nm+"/find_raw_material",
            dataType: "json",
            data:{
                key : $('#raw_material_name').val()
            },
            success: function (data) {
                response($.map(data, function (item) {
                    return {
                        label: item.item_id,
                        value: item.item_id,
                        sn: item.sn
                    };
                }));
            }
        });
    },
    minLength: 1,
    select: function (event, ui) {
        $("#raw_material_id").val('');
        $("#raw_material_id").val(ui.item.sn);
    }
});
</script>
@endsection