@extends('layout.layouts')
@section('content')
<div class="panel panel-default" style="margin-left:20px">
<div class="panel-heading" style="margin-bottom:30px"><h3>Customer</h3></div>
  <div class="panel-body">
	 <form class="form-inline" id="frm_{{$tbl}}">
	 {{ csrf_field() }} 
		  <div class="form-group">
			<label for="uen">UEN</label>
			<input type="hidden" class="form-control" id="id" value="" name="id">
			<input type="text" class="form-control" id="uen" name="uen" placeholder="UEN" required>
		  </div>
		  <div class="form-group">
			<label for="company_name">Company Name</label>
			<div class="form-group">
			  <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" required>
			</div>
		  </div>
		  <div class="form-group">
			<label for="contact_person">Contact Person</label>
			<input type="text" class="form-control" id="contact_person" name="contact_person" placeholder="Contact Person" required>
		  </div>
		   <div class="form-group">
			<label for="mobile_no">Mobile Number</label>
			<input type="text" class="form-control" id="mobile_no" name="mobile_no" placeholder="Mobile Number" required>
		  </div>
		  <div class="form-group">
			<label for="fax">Fax</label>
			<input type="text" class="form-control" id="fax" name="fax" placeholder="Fax" required>
		  </div>
		  <div class="form-group">
			<label for="email">Email Address</label>
			<input type="text" class="form-control" id="email" name="email" placeholder="Email Address" required>
		  </div>
         <div class="form-group">
                <label for="address">Company Address</label>
                <div class="form-group">
                    <textarea style="margin-bottom: -10px;" rows="4" cols="35" class="form-control"  id="company_add" name="company_add" placeholder="Company Address" required></textarea>
                </div>
         </div>
		  <div class="form-group">
			<label for="name">Country</label>
              <input type="hidden" id="country_id" name="country_id" required>
			  <input type="text" class="form-control" id="country_name" placeholder="Type to find" required>
		  </div>
		  <div class="form-group">
			<label for="name">Postal Code</label>
			<input type="text" class="form-control" id="postal" name="postal" placeholder="Postal Code" required>
		  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
	</form> 
	<table class="table table-hover" id="tbl_{{$tbl}}">
	  <thead>
		<tr>
		  <th scope="col">#</th>
		  <th scope="col">Action</th>
          <th scope="col">UEN</th>
		  <th scope="col">Company Name</th>
		  <th scope="col">Contact Person</th>
		  <th scope="col">Mobile Number</th>
		  <th scope="col">Fax</th>
		  <th scope="col">Email Address</th>
		  <th scope="col">Company Address</th>
		  <th scope="col">Country</th>
		  <th scope="col">Postal Code</th>
		</tr>
	  </thead>
	  <tbody>
	  </tbody>
	</table>
  </div>
</div>
<script>
var nm = '{{$tbl}}';
var tbl = "tbl_"+nm;
var frm = "frm_"+nm;

$("#"+frm).submit(function(event){
    event.preventDefault();
	 var form = $(this);

    $.ajax({
           type: "POST",
           url: nm+'/insert',
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
			   
			   display_success();
               load_data();
           }
    });
});

function load_data(){
	
	 $.ajax({
           type: "POST",
           url: nm+'/load_customer',
		   data :{
			   "_token": "{{ csrf_token() }}",
		   },
           success: function(data)
           {
      	   var table = "";
			   $.each(data, function( index, value ) {
				  table += "<tr>";
				  table += "<td>"+(index+1)+"</td>";
				  table +='<td><button type="button" class="btn btn-success btn-sm" onclick="get_by('+value.id+')" style="margin-right:10px"><span class="glyphicon glyphicon-pencil"></span> Edit';
				  table +='</button><button type="button" class="btn btn-danger btn-sm" onclick="soft_delete('+value.id+')"><span class="glyphicon glyphicon-trash"></span> Delete';
				  table +='</button></td>';
				  table += "<td>"+value.uen+"</td>";
				  table += "<td>"+value.company_name+"</td>";
				  table += "<td>"+value.contact_person+"</td>";
				  table += "<td>"+value.mobile_no+"</td>";
				  table += "<td>"+value.fax+"</td>";
				  table += "<td>"+value.email+"</td>";
				  table += "<td>"+value.company_add+"</td>";
				  table += "<td>"+value.country_name+"</td>";
			      table += "<td>"+value.postal+"</td>";
				  table += "</tr>";
			   });
			   
			   $('#'+tbl+' > tbody').html(table);
			   
			   
			   
           }
    });
}

function soft_delete(id_data=''){
	
	$.ajax({
           type: "POST",
           url: nm+"/soft_delete",
           data: {id : id_data,"_token": "{{ csrf_token() }}"},
           success: function(data)
           {	
	           load_data();
           }
    });
}

function get_by(id_data=''){
	
	$.ajax({
           type: "POST",
           url: nm+'/get_customer',
           data: {id : id_data,"_token": "{{ csrf_token() }}"},
           success: function(data)
           {	
	     	   $.each(data[0], function( k, v ) {
				  $('#'+frm+' #'+k).val(v)
			   });
			   
	       }
    });
}

$("#country_name").autocomplete({
    source: function (request, response) {
        $.ajax({
            type: "POST",
            url: nm+"/find_country",
            dataType: "json",
            data:{
                key : $('#country_name').val(),"_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                response($.map(data, function (item) {
                    return {
                        label: item.name,
                        value: item.name,
                        id: item.id
                    };
                }));
            }
        });
    },
    minLength: 1,
    select: function (event, ui) {
        $("#country_id").val("");
        $("#country_id").val(ui.item.id);
    }
});


load_data();
</script>
@endsection