@extends('layout.layouts')
@section('content')
<div class="panel panel-default" style="margin-left:20px">
<div class="panel-heading" style="margin-bottom:30px"><h3>Raw Material</h3></div>
  <div class="panel-body">
	 <form class="form-inline" id="frm_{{$tbl}}">
	 {{ csrf_field() }} 
		  <div class="form-group">
            <label for="item_category_id">Item Category</label>
            <div class="form-group">
                <input type="hidden" class="form-control" id="id" value="" name="id">
                <input type="hidden" class="form-control" id="item_category_id" name="item_category_id" >
                <input type="text" class="form-control" id="item_category_name" style="margin-bottom: -20px" placeholder="Type to find">
            </div>
          </div>
        <div class="form-group">
            <label for="name">Specific Item Name</label>
            <input type="hidden" class="form-control" id="specific_item_name_id" name="specific_item_name_id">
            <input type="text" class="form-control" id="specific_item_name" placeholder="Type to find">
        </div>
		  <div class="form-group">
			<label for="item_id">Item ID</label>
			<input type="text" class="form-control" id="item_id" name="item_id" placeholder="Item ID">
		  </div>
		  <div class="form-group">
			<label for="desc">Description</label>
			<input type="text" class="form-control" id="desc" name="desc" placeholder="Description">
		  </div>
		  <div class="form-group">
			<label for="qty">Qty</label>
			<input type="number" class="form-control" id="qty" name="qty" placeholder="Qty">
		  </div>
		   <div class="form-group">
			<label for="remarks">Remarks</label>
			<input type="text" class="form-control" id="remarks" name="remarks" placeholder="Remarks">
		  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
	</form> 
	<table class="table table-hover" id="tbl_{{$tbl}}">
	  <thead>
		<tr>
		  <th scope="col">#</th>
		  <th scope="col">Action</th>
		  <th scope="col">Category</th>
          <th scope="col">Name</th>
          <th scope="col">Item ID</th>
		  <th scope="col">Description</th>
		  <th scope="col">Qty</th>
		  <th scope="col">Remarks</th>
		</tr>
	  </thead>
	  <tbody>
	  </tbody>
	</table>
  </div>
</div>
<script>
var nm = '{{$tbl}}';
var tbl = "tbl_"+nm;
var frm = "frm_"+nm;

$("#"+frm).submit(function(event){
    event.preventDefault();
	 var form = $(this);

    $.ajax({
           type: "POST",
           url: nm+'/insert',
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
			   
			   display_success();
               load_data();
           }
    });
});

function load_data(){
	
	 $.ajax({
           type: "POST",
           url: nm+'/load_raw_material',
		   data :{
			   "_token": "{{ csrf_token() }}",
		   },
           success: function(data)
           {
      	   var table = "";
			   $.each(data, function( index, value ) {
				  table += "<tr>";
				  table += "<td>"+(index+1)+"</td>";
				  table +='<td><button type="button" class="btn btn-success btn-sm" onclick="get_by('+value.id+')" style="margin-right:10px"><span class="glyphicon glyphicon-pencil"></span> Edit';
				  table +='</button><button type="button" class="btn btn-danger btn-sm" onclick="soft_delete('+value.id+')"><span class="glyphicon glyphicon-trash"></span> Delete';
				  table +='</button></td>';
				  table += "<td>"+value.item_category_name+"</td>";
                  table += "<td>"+value.specific_item_name+"</td>";
                  table += "<td>"+value.item_id+"</td>";
				  table += "<td>"+value.desc+"</td>";
				  table += "<td>"+value.qty+"</td>";
				  table += "<td>"+value.remarks+"</td>";
				  table += "</tr>";
			   });
			   
			   $('#'+tbl+' > tbody').html(table);
			   
			   
			   
           }
    });
}

function soft_delete(id_data=''){
	
	$.ajax({
           type: "POST",
           url: nm+"/soft_delete",
           data: {id : id_data,"_token": "{{ csrf_token() }}"},
           success: function(data)
           {	
	           load_data();
           }
    });
}

function get_by(id_data=''){
	
	$.ajax({
           type: "POST",
           url: nm+'/get_raw_material',
           data: {id : id_data,"_token": "{{ csrf_token() }}"},
           success: function(data)
           {	
	     	   $.each(data[0], function( k, v ) {
				  $('#'+frm+' #'+k).val(v)
			   });
			   
	       }
    });
}

$("#item_category_name").autocomplete({
    source: function (request, response) {
        $.ajax({
            type: "POST",
            url: nm+"/find_item_category",
            dataType: "json",
            data:{
                key : $('#item_category_name').val(),"_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                response($.map(data, function (item) {
                    return {
                        label: item.name,
                        value: item.name,
                        id: item.id
                    };
                }));
            }
        });
    },
    minLength: 1,
    select: function (event, ui) {
        $("#item_category_id").val("");
        $("#item_category_id").val(ui.item.id);
    }
});

$("#specific_item_name").autocomplete({
    source: function (request, response) {
        $.ajax({
            type: "POST",
            url: nm+'/find_specific_item_name',
            dataType: "json",
            data:{
                key : $('#specific_item_name').val(),"_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                response($.map(data, function (item) {
                    return {
                        label: item.name,
                        value: item.name,
                        id: item.id
                    };
                }));
            }
        });
    },
    minLength: 1,
    select: function (event, ui) {
        $("#specific_item_name_id").val("");
        $("#specific_item_name_id").val(ui.item.id);
    }
});

load_data();
</script>
@endsection