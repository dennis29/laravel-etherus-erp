@extends('layout.layouts')
@section('content')
<div class="panel panel-default" style="margin-left:20px">
<div class="panel-heading" style="margin-bottom:30px"><h3>Department Category</h3></div>
  <div class="panel-body">
	 <form class="form-inline" id="frm_{{$tbl}}">
	 {{ csrf_field() }} 
		  <div class="form-group">
			<label for="name">Name</label>
			<input type="hidden" class="form-control" id="id" value="" name="id">
			<input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
		  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
	</form> 
	<table class="table table-hover" id="tbl_{{$tbl}}">
	  <thead>
		<tr>
		  <th scope="col">#</th>
		  <th scope="col">Action</th>
		  <th scope="col">Name</th>
		</tr>
	  </thead>
	  <tbody>
	  </tbody>
	</table>
  </div>
</div>
<script>
var nm = '{{$tbl}}';
var tbl = "tbl_"+nm;
var frm = "frm_"+nm;

$("#"+frm).submit(function(event){
    event.preventDefault();
	 var form = $(this);

    $.ajax({
           type: "POST",
           url: nm+'/insert',
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
			   
			   display_success();
               load_data();
           }
    });
});

function load_data(){
	
	 $.ajax({
           type: "POST",
           url: nm+'/load_data',
		   data :{
			   "_token": "{{ csrf_token() }}",
		   },
           success: function(data)
           {
      	   var table = "";
			   $.each(data, function( index, value ) {
				  table += "<tr>";
				  table += "<td>"+(index+1)+"</td>";
				  table +='<td><button type="button" class="btn btn-success btn-sm" onclick="get_by('+value.id+')" style="margin-right:10px"><span class="glyphicon glyphicon-pencil"></span> Edit';
				  table +='</button><button type="button" class="btn btn-danger btn-sm" onclick="soft_delete('+value.id+')"><span class="glyphicon glyphicon-trash"></span> Delete';
				  table +='</button></td>';
				  table += "<td>"+value.name+"</td>";
				  table += "</tr>";
			   });
			   
			   $('#'+tbl+' > tbody').html(table);
			   
			   
			   
           }
    });
}

function soft_delete(id_data=''){
	
	$.ajax({
           type: "POST",
           url: nm+"/soft_delete",
           data: {id : id_data,"_token": "{{ csrf_token() }}"},
           success: function(data)
           {	
	           load_data();
           }
    });
}

function get_by(id_data=''){
	
	$.ajax({
           type: "POST",
           url: nm+'/get_by',
           data: {id : id_data,"_token": "{{ csrf_token() }}"},
           success: function(data)
           {	
	     	   $.each(data[0], function( k, v ) {
				  $('#'+frm+' #'+k).val(v)
			   });
			   
	       }
    });
}


load_data();
</script>
@endsection