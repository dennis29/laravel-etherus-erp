<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('uen',100);
			$table->string('company_name',100);
			$table->string('sup_name',100);
			$table->string('mobile_no',100);
			$table->string('fax',100);
			$table->string('email',100);
			$table->string('company_add',300);
			$table->string('remarks',100);
			$table->string('gst',100);
			$table->string('postal',100);
			$table->string('status_data',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
