<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('proj_name',400);
			$table->string('task_priority',200);
			$table->string('task_desc',200);
			$table->date('start_date');
			$table->date('end_date');
			$table->string('created_by',40);
			$table->string('assign_employee',40);
			$table->string('status',40);
			$table->string('status_data',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
