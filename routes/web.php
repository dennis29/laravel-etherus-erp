<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@main_index')->name('main_index');

Route::group([
			'prefix' => 'marital_statuses',
				], function() {
				  Route::get('', 'MaritalStatusController@main_index');
				  Route::post('soft_delete', 'MaritalStatusController@soft_delete');
				  Route::post('get_by', 'MaritalStatusController@get_by');
				  Route::post('load_data', 'MaritalStatusController@load_data');
				  Route::post('insert', 'MaritalStatusController@insert');
});

Route::group([
			'prefix' => 'nationalities',
				], function() {
				  Route::get('', 'NationalityController@main_index');
				  Route::post('soft_delete', 'NationalityController@soft_delete');
				  Route::post('get_by', 'NationalityController@get_by');
				  Route::post('load_data', 'NationalityController@load_data');
				  Route::post('insert', 'NationalityController@insert');
});

Route::group([
			'prefix' => 'religions',
				], function() {
				  Route::get('', 'ReligionController@main_index');
				  Route::post('soft_delete', 'ReligionController@soft_delete');
				  Route::post('get_by', 'ReligionController@get_by');
				  Route::post('load_data', 'ReligionController@load_data');
				  Route::post('insert', 'ReligionController@insert');
});

Route::group([
			'prefix' => 'ethnics_groups',
				], function() {
				  Route::get('', 'EthnicsGroupController@main_index');
				  Route::post('soft_delete', 'EthnicsGroupController@soft_delete');
				  Route::post('get_by', 'EthnicsGroupController@get_by');
				  Route::post('load_data', 'EthnicsGroupController@load_data');
				  Route::post('insert', 'EthnicsGroupController@insert');
});

Route::group([
			'prefix' => 'department_categories',
				], function() {
				  Route::get('', 'DepartmentCategoryController@main_index');
				  Route::post('soft_delete', 'DepartmentCategoryController@soft_delete');
				  Route::post('get_by', 'DepartmentCategoryController@get_by');
				  Route::post('load_data', 'DepartmentCategoryController@load_data');
				  Route::post('insert', 'DepartmentCategoryController@insert');
});

Route::group([
			'prefix' => 'employement_titles',
				], function() {
				  Route::get('', 'EmployementTitleController@main_index');
				  Route::post('soft_delete', 'EmployementTitleController@soft_delete');
				  Route::post('get_by', 'EmployementTitleController@get_by');
				  Route::post('load_data', 'EmployementTitleController@load_data');
				  Route::post('insert', 'EmployementTitleController@insert');
});

Route::group([
			'prefix' => 'employement_types',
				], function() {
				  Route::get('', 'EmployementTypeController@main_index');
				  Route::post('soft_delete', 'EmployementTypeController@soft_delete');
				  Route::post('get_by', 'EmployementTypeController@get_by');
				  Route::post('load_data', 'EmployementTypeController@load_data');
				  Route::post('insert', 'EmployementTypeController@insert');
});

Route::group([
			'prefix' => 'qualifications_types',
				], function() {
				  Route::get('', 'QualificationsTypeController@main_index');
				  Route::post('soft_delete', 'QualificationsTypeController@soft_delete');
				  Route::post('get_by', 'QualificationsTypeController@get_by');
				  Route::post('load_data', 'QualificationsTypeController@load_data');
				  Route::post('insert', 'QualificationsTypeController@insert');
});

Route::group([
			'prefix' => 'academy_types',
				], function() {
				  Route::get('', 'AcademyTypeController@main_index');
				  Route::post('soft_delete', 'AcademyTypeController@soft_delete');
				  Route::post('get_by', 'AcademyTypeController@get_by');
				  Route::post('load_data', 'AcademyTypeController@load_data');
				  Route::post('insert', 'AcademyTypeController@insert');
});

Route::group([
			'prefix' => 'genders',
				], function() {
				  Route::get('', 'GenderController@main_index');
				  Route::post('soft_delete', 'GenderController@soft_delete');
				  Route::post('get_by', 'GenderController@get_by');
				  Route::post('load_data', 'GenderController@load_data');
				  Route::post('insert', 'GenderController@insert');
});

Route::group([
			'prefix' => 'countries',
				], function() {
				  Route::get('', 'CountryController@main_index');
				  Route::post('soft_delete', 'CountryController@soft_delete');
				  Route::post('get_by', 'CountryController@get_by');
				  Route::post('load_data', 'CountryController@load_data');
				  Route::post('insert', 'CountryController@insert');
});

Route::group([
			'prefix' => 'attendance_settings',
				], function() {
				  Route::get('', 'AttendanceSettingController@main_index');
				  Route::post('soft_delete', 'AttendanceSettingController@soft_delete');
				  Route::post('get_by', 'AttendanceSettingController@get_by');
				  Route::post('load_data', 'AttendanceSettingController@load_data');
				  Route::post('insert', 'AttendanceSettingController@insert');
});

Route::group([
			'prefix' => 'claims_categories',
				], function() {
				  Route::get('', 'ClaimsCategoryController@main_index');
				  Route::post('soft_delete', 'ClaimsCategoryController@soft_delete');
				  Route::post('get_by', 'ClaimsCategoryController@get_by');
				  Route::post('load_data', 'ClaimsCategoryController@load_data');
				  Route::post('insert', 'ClaimsCategoryController@insert');
});

Route::group([
			'prefix' => 'leave_categories',
				], function() {
				  Route::get('', 'LeaveCategoryController@main_index');
				  Route::post('soft_delete', 'LeaveCategoryController@soft_delete');
				  Route::post('get_by', 'LeaveCategoryController@get_by');
				  Route::post('load_data', 'LeaveCategoryController@load_data');
				  Route::post('insert', 'LeaveCategoryController@insert');
});

Route::group([
			'prefix' => 'appraisal_settings',
				], function() {
				  Route::get('', 'AppraisalSettingController@main_index');
				  Route::post('soft_delete', 'AppraisalSettingController@soft_delete');
				  Route::post('get_by', 'AppraisalSettingController@get_by');
				  Route::post('load_data', 'AppraisalSettingController@load_data');
				  Route::post('insert', 'AppraisalSettingController@insert');
});

Route::group([
			'prefix' => 'item_categories',
				], function() {
				  Route::get('', 'ItemCategoryController@main_index');
				  Route::post('soft_delete', 'ItemCategoryController@soft_delete');
				  Route::post('get_by', 'ItemCategoryController@get_by');
				  Route::post('load_data', 'ItemCategoryController@load_data');
				  Route::post('insert', 'ItemCategoryController@insert');
});

Route::group([
			'prefix' => 'frequency_settings',
				], function() {
				  Route::get('', 'FrequencySettingController@main_index');
				  Route::post('soft_delete', 'FrequencySettingController@soft_delete');
				  Route::post('get_by', 'FrequencySettingController@get_by');
				  Route::post('load_data', 'FrequencySettingController@load_data');
				  Route::post('insert', 'FrequencySettingController@insert');
});

Route::group([
			'prefix' => 'message_target_settings',
				], function() {
				  Route::get('', 'MessageTargetSettingController@main_index');
				  Route::post('soft_delete', 'MessageTargetSettingController@soft_delete');
				  Route::post('get_by', 'MessageTargetSettingController@get_by');
				  Route::post('load_data', 'MessageTargetSettingController@load_data');
				  Route::post('insert', 'MessageTargetSettingController@insert');
});

Route::group([
			'prefix' => 'specific_item_names',
				], function() {
				  Route::get('', 'SpecificItemNameController@main_index');
				  Route::post('soft_delete', 'SpecificItemNameController@soft_delete');
				  Route::post('get_by', 'SpecificItemNameController@get_by');
				  Route::post('load_data', 'SpecificItemNameController@load_data');
				  Route::post('insert', 'SpecificItemNameController@insert');
});

Route::group([
			'prefix' => 'warehouses',
				], function() {
				  Route::get('', 'WarehouseController@main_index');
				  Route::post('soft_delete', 'WarehouseController@soft_delete');
				  Route::post('get_by', 'WarehouseController@get_by');
				  Route::post('load_data_warehouse', 'WarehouseController@load_data_warehouse');
				  Route::post('insert', 'WarehouseController@insert');
				  Route::post('find_country', 'WarehouseController@find_country');
				  Route::post('get_warehouse', 'WarehouseController@get_warehouse');
});

Route::group([
			'prefix' => 'tasks',
				], function() {
				  Route::get('', 'TaskController@main_index');
				  Route::post('soft_delete', 'TaskController@soft_delete');
				  Route::post('get_by', 'TaskController@get_by');
				  Route::post('load_data', 'TaskController@load_data');
				  Route::post('insert', 'TaskController@insert');
});

Route::group([
			'prefix' => 'suppliers',
				], function() {
				  Route::get('', 'SupplierController@main_index');
				  Route::post('soft_delete', 'SupplierController@soft_delete');
				  Route::post('get_by', 'SupplierController@get_by');
				  Route::post('load_data', 'SupplierController@load_data');
				  Route::post('insert', 'SupplierController@insert');
});

Route::group([
			'prefix' => 'raw_materials',
				], function() {
				  Route::get('', 'RawMaterialController@main_index');
				  Route::post('soft_delete', 'RawMaterialController@soft_delete');
				  Route::post('get_raw_material', 'RawMaterialController@get_raw_material');
				  Route::post('load_raw_material', 'RawMaterialController@load_raw_material');
				  Route::post('insert', 'RawMaterialController@insert');
				  Route::post('find_item_category', 'RawMaterialController@find_item_category');
				  Route::post('find_specific_item_name', 'RawMaterialController@find_specific_item_name');
});

Route::group([
			'prefix' => 'purchase_orders',
				], function() {
				  Route::get('', 'PurchaseOrderController@main_index');
				  Route::post('soft_delete', 'PurchaseOrderController@soft_delete');
				  Route::post('get_purchase_order', 'PurchaseOrderController@get_purchase_order');
				  Route::post('load_purchase_order', 'PurchaseOrderController@load_purchase_order');
				  Route::post('insert', 'PurchaseOrderController@insert');
				  Route::post('find_supplier', 'PurchaseOrderController@find_supplier');
});

Route::group([
			'prefix' => 'motor_types',
				], function() {
				  Route::get('', 'MotorTypeController@main_index');
				  Route::post('soft_delete', 'MotorTypeController@soft_delete');
				  Route::post('get_by', 'MotorTypeController@get_by');
				  Route::post('load_data', 'MotorTypeController@load_data');
				  Route::post('insert', 'MotorTypeController@insert');
});

Route::group([
			'prefix' => 'customers',
				], function() {
				  Route::get('', 'CustomerController@main_index');
				  Route::post('soft_delete', 'CustomerController@soft_delete');
				  Route::post('get_customer', 'CustomerController@get_customer');
				  Route::post('load_customer', 'CustomerController@load_customer');
				  Route::post('insert', 'CustomerController@insert');
				  Route::post('find_country', 'CustomerController@find_country');
});

Route::group([
			'prefix' => 'purchase_order_lists',
				], function() {
				  Route::get('', 'PurchaseOrderListController@main_index');
				  Route::post('soft_delete', 'PurchaseOrderListController@soft_delete');
				  Route::post('find_purchase_order', 'PurchaseOrderListController@find_purchase_order');
				  Route::post('get_by', 'PurchaseOrderListController@get_by');
				  Route::post('load_data_per_purchase_order_list', 'PurchaseOrderListController@load_data_per_purchase_order_list');
				  Route::post('insert', 'PurchaseOrderListController@insert');
});