INSERT INTO academy_types VALUES (1, 'University', '1', NULL, NULL);
INSERT INTO academy_types VALUES (2, 'PolyTechnic', '1', NULL, NULL);
INSERT INTO academy_types VALUES (3, 'ITE', '1', NULL, NULL);
INSERT INTO academy_types VALUES (4, 'Junior College', '1', NULL, NULL);
INSERT INTO academy_types VALUES (5, 'Secondary School', '1', NULL, NULL);
INSERT INTO academy_types VALUES (6, 'Primary School', '1', NULL, NULL);


--
-- Data for Name: appraisal_settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO appraisal_settings VALUES (1, 'What Is your name?', '1', NULL, NULL);
INSERT INTO appraisal_settings VALUES (2, 'Where you live?', '1', NULL, NULL);


--
-- Data for Name: attendance_settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO attendance_settings VALUES (1, 'Presence', '1', NULL, NULL);
INSERT INTO attendance_settings VALUES (2, 'Absence', '1', NULL, NULL);
INSERT INTO attendance_settings VALUES (3, 'MC', '1', NULL, NULL);
INSERT INTO attendance_settings VALUES (4, 'On Leave', '1', NULL, NULL);
INSERT INTO attendance_settings VALUES (5, 'Off-In-Lieu', '1', NULL, NULL);


--
-- Data for Name: claims_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO claims_categories VALUES (1, 'Pregnant', '1', NULL, NULL);
INSERT INTO claims_categories VALUES (2, 'Surgery', '1', NULL, NULL);
INSERT INTO claims_categories VALUES (3, 'Medical exams', '1', NULL, NULL);
INSERT INTO claims_categories VALUES (4, 'Medical checkup', '1', NULL, NULL);


--
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO countries VALUES (1, 'Indonesia', '1', NULL, NULL);
INSERT INTO countries VALUES (2, 'Malaysia', '1', NULL, NULL);
INSERT INTO countries VALUES (3, 'Singapore', '1', NULL, NULL);
INSERT INTO countries VALUES (4, 'Cambodia', '1', NULL, NULL);
INSERT INTO countries VALUES (5, 'Thailand', '1', NULL, NULL);


--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO customers VALUES (2, '2127127217', 'DMX', '+62812727', '+62812727', '+62812727', 'dm@yahoo.com', 'West Jakarta', 2, '1822', '1', NULL, NULL);


--
-- Data for Name: department_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO department_categories VALUES (1, 'IT', '1', NULL, NULL);
INSERT INTO department_categories VALUES (2, 'Marketing', '1', NULL, NULL);
INSERT INTO department_categories VALUES (3, 'Production', '1', NULL, NULL);
INSERT INTO department_categories VALUES (4, 'R&D', '1', NULL, NULL);


--
-- Data for Name: employement_titles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO employement_titles VALUES (1, 'Accounting', '1', NULL, NULL);
INSERT INTO employement_titles VALUES (2, 'Auditor', '1', NULL, NULL);
INSERT INTO employement_titles VALUES (3, 'Business Analyst', '1', NULL, NULL);
INSERT INTO employement_titles VALUES (4, 'Junior Programmer', '1', NULL, NULL);
INSERT INTO employement_titles VALUES (5, 'Software Developer', '1', NULL, NULL);


--
-- Data for Name: employement_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO employement_types VALUES (1, 'Part Time Contract', '1', NULL, NULL);
INSERT INTO employement_types VALUES (2, 'Full Time Permanent', '1', NULL, NULL);
INSERT INTO employement_types VALUES (3, 'Part Time Permanent', '1', NULL, NULL);


--
-- Data for Name: ethnics_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ethnics_groups VALUES (1, 'Chinese', '1', NULL, NULL);
INSERT INTO ethnics_groups VALUES (2, 'Malaysian', '1', NULL, NULL);
INSERT INTO ethnics_groups VALUES (3, 'Indians', '1', NULL, NULL);
INSERT INTO ethnics_groups VALUES (4, 'Eurasians', '1', NULL, NULL);
INSERT INTO ethnics_groups VALUES (5, 'Others', '1', NULL, NULL);


--
-- Data for Name: frequency_settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO frequency_settings VALUES (1, 'Daily', '1', NULL, NULL);
INSERT INTO frequency_settings VALUES (2, 'Weekly', '1', NULL, NULL);
INSERT INTO frequency_settings VALUES (3, 'Fortnightly', '1', NULL, NULL);
INSERT INTO frequency_settings VALUES (4, 'Monthly', '1', NULL, NULL);


--
-- Data for Name: genders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO genders VALUES (1, 'Male', '1', NULL, NULL);
INSERT INTO genders VALUES (2, 'Female', '1', NULL, NULL);


--
-- Data for Name: item_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO item_categories VALUES (1, 'Curtain Fabric', '1', NULL, NULL);
INSERT INTO item_categories VALUES (2, 'Blinds', '1', NULL, NULL);


--
-- Data for Name: leave_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO leave_categories VALUES (1, 'Pregnant', '1', NULL, NULL);
INSERT INTO leave_categories VALUES (2, 'Menstrual leave', '1', NULL, NULL);
INSERT INTO leave_categories VALUES (3, 'Parental leave', '1', NULL, NULL);


--
-- Data for Name: marital_statuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO marital_statuses VALUES (1, 'Single', '1', NULL, NULL);
INSERT INTO marital_statuses VALUES (2, 'Married', '1', NULL, NULL);
INSERT INTO marital_statuses VALUES (3, 'Divorced', '1', NULL, NULL);
INSERT INTO marital_statuses VALUES (4, 'Widow', '1', NULL, NULL);
INSERT INTO marital_statuses VALUES (5, 'Widower', '0', NULL, NULL);
INSERT INTO marital_statuses VALUES (6, 'Widower', '1', NULL, NULL);


--
-- Data for Name: message_target_settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO message_target_settings VALUES (1, 'mobile', '1', NULL, NULL);
INSERT INTO message_target_settings VALUES (2, 'phone', '1', NULL, NULL);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO migrations VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO migrations VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO migrations VALUES (3, '2019_12_05_090112_create_marital_statuses_table', 1);
INSERT INTO migrations VALUES (5, '2019_12_05_121626_create_nationalities_table', 2);
INSERT INTO migrations VALUES (6, '2019_12_05_123044_create_religions_table', 3);
INSERT INTO migrations VALUES (7, '2019_12_05_123917_create_ethnics_groups_table', 4);
INSERT INTO migrations VALUES (8, '2019_12_05_124654_create_department_categories_table', 5);
INSERT INTO migrations VALUES (9, '2019_12_05_125321_create_employement_titles_table', 6);
INSERT INTO migrations VALUES (11, '2019_12_05_130140_create_employement_types_table', 7);
INSERT INTO migrations VALUES (12, '2019_12_05_130939_create_qualifications_types_table', 8);
INSERT INTO migrations VALUES (13, '2019_12_05_131600_create_academy_types_table', 9);
INSERT INTO migrations VALUES (15, '2019_12_05_132206_create_genders_table', 10);
INSERT INTO migrations VALUES (17, '2019_12_05_135152_create_countries_table', 11);
INSERT INTO migrations VALUES (18, '2019_12_05_140004_create_attendance_settings_table', 12);
INSERT INTO migrations VALUES (20, '2019_12_05_140718_create_claims_categories_table', 13);
INSERT INTO migrations VALUES (21, '2019_12_05_141632_create_leave_categories_table', 14);
INSERT INTO migrations VALUES (23, '2019_12_05_142126_create_appraisal_settings_table', 15);
INSERT INTO migrations VALUES (24, '2019_12_05_142706_create_item_categories_table', 16);
INSERT INTO migrations VALUES (25, '2019_12_06_080254_create_frequency_settings_table', 17);
INSERT INTO migrations VALUES (26, '2019_12_06_081259_create_message_target_settings_table', 18);
INSERT INTO migrations VALUES (27, '2019_12_06_081756_create_specific_item_names_table', 19);
INSERT INTO migrations VALUES (28, '2019_12_06_082453_create_warehouses_table', 20);
INSERT INTO migrations VALUES (29, '2019_12_06_103110_create_tasks_table', 21);
INSERT INTO migrations VALUES (30, '2019_12_07_125440_create_suppliers_table', 22);
INSERT INTO migrations VALUES (32, '2019_12_07_130900_create_raw_materials_table', 23);
INSERT INTO migrations VALUES (33, '2019_12_07_135630_create_purchase_orders_table', 24);
INSERT INTO migrations VALUES (34, '2019_12_07_143832_create_motor_types_table', 25);
INSERT INTO migrations VALUES (35, '2019_12_07_144606_create_customers_table', 26);


--
-- Data for Name: motor_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO motor_types VALUES (1, 'MMW', '1', NULL, NULL);
INSERT INTO motor_types VALUES (2, 'GHN', '1', NULL, NULL);


--
-- Data for Name: nationalities; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO nationalities VALUES (1, 'Indonesian', '1', NULL, NULL);
INSERT INTO nationalities VALUES (2, 'Singaporean', '1', NULL, NULL);
INSERT INTO nationalities VALUES (3, 'Malaysian', '1', NULL, NULL);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: purchase_orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO purchase_orders VALUES (1, 'JK182828', 1, '1', NULL, NULL);


--
-- Data for Name: qualifications_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO qualifications_types VALUES (1, 'PhD', '1', NULL, NULL);
INSERT INTO qualifications_types VALUES (2, 'Masters', '1', NULL, NULL);
INSERT INTO qualifications_types VALUES (3, 'Bachelor Science Degree', '1', NULL, NULL);
INSERT INTO qualifications_types VALUES (4, 'Bachelor Degree', '1', NULL, NULL);
INSERT INTO qualifications_types VALUES (5, 'Advanced Diploma', '1', NULL, NULL);
INSERT INTO qualifications_types VALUES (6, 'Diploma', '1', NULL, NULL);
INSERT INTO qualifications_types VALUES (7, 'NiTEC', '1', NULL, NULL);
INSERT INTO qualifications_types VALUES (8, '‘A’ Level', '1', NULL, NULL);
INSERT INTO qualifications_types VALUES (9, '‘O’ Level', '1', NULL, NULL);
INSERT INTO qualifications_types VALUES (10, '‘N’ Level', '1', NULL, NULL);
INSERT INTO qualifications_types VALUES (11, 'PSLE', '1', NULL, NULL);


--
-- Data for Name: raw_materials; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO raw_materials VALUES (1, '212112NJNN', 1, 1, 'Good', 12, 'Good', '1', NULL, NULL);


--
-- Data for Name: religions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO religions VALUES (1, 'Buddhism', '1', NULL, NULL);
INSERT INTO religions VALUES (2, 'Christians', '1', NULL, NULL);
INSERT INTO religions VALUES (3, 'Hindus', '1', NULL, NULL);
INSERT INTO religions VALUES (4, 'Muslims', '1', NULL, NULL);
INSERT INTO religions VALUES (5, 'Free-Thinker', '1', NULL, NULL);
INSERT INTO religions VALUES (6, 'Others', '1', NULL, NULL);


--
-- Data for Name: specific_item_names; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO specific_item_names VALUES (1, 'Mouse Laptop', '1', NULL, NULL);
INSERT INTO specific_item_names VALUES (2, 'Speaker', '1', NULL, NULL);


--
-- Data for Name: suppliers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO suppliers VALUES (1, '123123123', 'Axion', 'Sky', '+6487272727', '+6487272727', 'axion@yahoo.com', 'Indonesia Menteng Central Jakarta', 'dennis', '12828128', '172828', '1', NULL, NULL);


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tasks VALUES (1, 'X-Project', '1', 'Develope System HRD', '2019-12-10', '2019-12-12', 'Dennis', 'Maman', '3', '1', NULL, NULL);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: warehouses; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO warehouses VALUES (1, 3, 'West Jakarta 9 Cambodia Street', 'Good', 'Good', '1', NULL, NULL);


--
-- Name: academy_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres